package io.github.juanhdev.blog_pessoal.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import io.github.juanhdev.blog_pessoal.repository.UsuarioRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UsuarioControllerTest {

  @Autowired
  private TestRestTemplate testRestTemplate;

  private Usuario usuario;
  private Usuario usuarioUpdate;
  private Usuario usuarioAdmin;

  @Autowired
  private UsuarioRepository repo;

  @BeforeAll
  public void start() {
    LocalDate dataAdmin = LocalDate.parse(
      "1990-07-22",
      DateTimeFormatter.ofPattern("yyyy-MM-dd")
    );
    usuarioAdmin =
      new Usuario(
        0L,
        "Administrador",
        "admin",
        "admin@email.com.br",
        "admin123",
        dataAdmin
      );

    if (!repo.findByUsername(usuarioAdmin.getUsername()).isPresent()) {
      HttpEntity<Usuario> request = new HttpEntity<Usuario>(usuarioAdmin);
      testRestTemplate.exchange(
        "/users/register",
        HttpMethod.POST,
        request,
        Usuario.class
      );
    }

    LocalDate dataPost = LocalDate.parse(
      "2000-07-22",
      DateTimeFormatter.ofPattern("yyyy-MM-dd")
    );
    usuario =
      new Usuario(
        0L,
        "Paulo Antunes",
        "pau",
        "paulo@email.com.br",
        "13465278",
        dataPost
      );

    LocalDate dataPut = LocalDate.parse(
      "2000-07-22",
      DateTimeFormatter.ofPattern("yyyy-MM-dd")
    );
    usuarioUpdate =
      new Usuario(
        2L,
        "Paulo Antunes de Souza",
        "pau",
        "paulo_souza@email.com.br",
        "souza123",
        dataPut
      );
  }

  @Test
  @Order(1)
  @DisplayName("Register usuario")
  public void deveRealizarPostUsuario() {
    HttpEntity<Usuario> request = new HttpEntity<>(usuario);

    ResponseEntity<Usuario> response = testRestTemplate.exchange(
      "/users/register",
      HttpMethod.POST,
      request,
      Usuario.class
    );

    assertEquals(HttpStatus.CREATED, response.getStatusCode());
  }

  @Test
  @Order(2)
  @DisplayName("Get all users")
  public void deveMostrarTodosUsuarios() {
    ResponseEntity<String> response = testRestTemplate
      .withBasicAuth("admin", "admin123")
      .exchange("/users/all", HttpMethod.GET, null, String.class);

    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  @Order(3)
  @DisplayName("Update user")
  public void deveRealizarPutUsuario() {
    HttpEntity<Usuario> request = new HttpEntity<Usuario>(usuarioUpdate);

    ResponseEntity<Usuario> response = testRestTemplate
      .withBasicAuth("admin", "admin123")
      .exchange("/users/update", HttpMethod.PUT, request, Usuario.class);

    assertEquals(HttpStatus.OK, response.getStatusCode());
  }
}

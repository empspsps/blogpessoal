package io.github.juanhdev.blog_pessoal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Usuario {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotNull
  @Size(min = 3, max = 50)
  private String name;

  @NotNull
  @Size(min = 3, max = 100)
  private String username;

  @NotNull
  @Size(min = 5, max = 100)
  @Email
  private String email;

  @NotNull
  @Size(min = 12)
  private String password;

  @Column(name = "birthdate")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate birthdate;

  @OneToMany(mappedBy = "usuario", cascade = CascadeType.REMOVE)
  @JsonIgnoreProperties("usuario")
  private List<BlogPost> usuarioPosts;

  public Usuario(long id, String name, String username, String email, String password, LocalDate birthdate) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.birthdate = birthdate;
  }

  public Usuario() {
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public LocalDate getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(LocalDate birthdate) {
    this.birthdate = birthdate;
  }

  public List<BlogPost> getUserPosts() {
    return usuarioPosts;
  }

  public void setUserPosts(List<BlogPost> userPosts) {
    this.usuarioPosts = userPosts;
  }
}

package io.github.juanhdev.blog_pessoal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Topic {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @NotNull(message = "Name required")
  private String name;

  @NotNull(message = "Description required")
  private String description;

  @OneToMany(mappedBy = "topic", cascade = CascadeType.ALL)
  @JsonIgnoreProperties("theme")
  private List<BlogPost> blogPost;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<BlogPost> getBlogPost() {
    return blogPost;
  }

  public void setBlogPost(List<BlogPost> blogPost) {
    this.blogPost = blogPost;
  }
}

package io.github.juanhdev.blog_pessoal.controller;

import io.github.juanhdev.blog_pessoal.model.BlogPost;
import io.github.juanhdev.blog_pessoal.repository.BlogPostRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/posts")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BlogPostController {

  @Autowired
  private BlogPostRepository repo;

  @GetMapping
  public ResponseEntity<List<BlogPost>> GetAll() {
    return ResponseEntity.ok(repo.findAll());
  }

  // Get post by id
  @GetMapping("/{id}")
  public ResponseEntity<BlogPost> getById(@PathVariable long id) {
    return repo
      .findById(id)
      .map(ResponseEntity::ok)
      .orElse(ResponseEntity.notFound().build());
  }

  // Get post by title
  @GetMapping("/title/{title}")
  public ResponseEntity<List<BlogPost>> getByTitulo(
    @PathVariable String title
  ) {
    return ResponseEntity.ok(repo.findAllByTitleContainingIgnoreCase(title));
  }

  // Create post
  @PostMapping
  public ResponseEntity<BlogPost> postPostagem(@RequestBody BlogPost blogpost) {
    return ResponseEntity.status(HttpStatus.CREATED).body(repo.save(blogpost));
  }

  // Edit post
  @PutMapping
  public ResponseEntity<BlogPost> putPostagem(@RequestBody BlogPost blogpost) {
    return ResponseEntity.status(HttpStatus.OK).body(repo.save(blogpost));
  }

  // Delete post
  @DeleteMapping("/{id}")
  public void deletePostagem(@PathVariable long id) {
    repo.deleteById(id);
  }
}

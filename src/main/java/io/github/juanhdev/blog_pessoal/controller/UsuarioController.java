package io.github.juanhdev.blog_pessoal.controller;

import io.github.juanhdev.blog_pessoal.model.Usuario;
import io.github.juanhdev.blog_pessoal.model.UsuarioLogin;
import io.github.juanhdev.blog_pessoal.repository.UsuarioRepository;
import io.github.juanhdev.blog_pessoal.service.UserService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsuarioController {

  @Autowired
  private UsuarioRepository repo;

  @Autowired
  private UserService service;

  @GetMapping("/all")
  public ResponseEntity<List<Usuario>> getAll() {
    return ResponseEntity.ok(repo.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Usuario> getById(@PathVariable long id) {
    return repo.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
  }

  @PostMapping("/login")
  public ResponseEntity<UsuarioLogin> login(@RequestBody Optional<UsuarioLogin> user) {
    return service.loginUser(user).map(ResponseEntity::ok)
        .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
  }

  @PostMapping("/register")
  public ResponseEntity<Usuario> register(@RequestBody Usuario user) {
    Usuario newUser = service.registerUser(user);
    try {
      return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping("/update")
  public ResponseEntity<Usuario> update(@RequestBody Usuario user) {
    Optional<Usuario> updatedUser = service.updateUser(user);
    try {
      return ResponseEntity.ok(updatedUser.get());
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
  }
  // Métodos Delete

  @DeleteMapping("/delete_id/{id}")
  public void deleteUser(@PathVariable long id) {
    repo.deleteById(id);
  }

  @DeleteMapping("/delete/{username}")
  public void deleteUserByUsername(@PathVariable String username) {
    Usuario userToDelete = repo.findByUsername(username).get();
    repo.deleteById(userToDelete.getId());
  }
}

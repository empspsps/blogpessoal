package io.github.juanhdev.blog_pessoal.repository;

import io.github.juanhdev.blog_pessoal.model.BlogPost;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogPostRepository extends JpaRepository<BlogPost, Long> {
  List<BlogPost> findAllByTitleContainingIgnoreCase(String title);
}

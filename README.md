# Meu blog pessoal

✅ Back-end finalizado   
🕗 Front-end em desenvolvimento

## ✅ Deploy realizado

Link para o projeto: https://juanhblog.herokuapp.com/swagger-ui/

### Controller de Posts
<img src="https://i.imgur.com/PfjEq9N.jpeg" width=290px>

### Controller de Usuário
<img src="https://i.imgur.com/BQJ0zyR.jpeg" width=290px>
